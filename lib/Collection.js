/** @module Collection*/

/**
* Represent a Collection object
* @implements Iterable
* @see {@link https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/iterable|Iterable}
*/
class Collection {

	/**
	* @constructor
	* @param {PlainObject|Array} items Initial items
	* @param {String} key Indexation key. If no key is provided, numeric key will be used.
	* @param {Boolean} track should Items be tracked for key change
	*/
	constructor(items=null, key, track=false)
	{
		this.items = (items||{});
		this.track = track;
		
		Object.defineProperty(this, 'key', {
			value : (key == void 0)?null:key,
		});
	}

	/**
	* Add item to collection.
	* @param {Object} item - Item to add
	*/
	add(item)
	{
		if(item == void 0)
			return;

		let idxKey 
		if ( (idxKey = this.key) !== null)
		{
			if(! idxKey in item)
				throw new Exception(`Cannot index item by [${idxKey}]`);

			
			this.items[item[idxKey]] = !this.track ? item : new Proxy(item, { 
				set: (target, prop, value, receiver) => {

					if (prop == idxKey) {
						this.move(target[prop], value)
					}

					target[prop] = value
					return true
				}
			});
		}
		else
			this.items[this.length] = item;

		return this;
	}

	/**
	 * Array.push emulation
	 * @param {*} item 
	 */
	push(item) {
		return this.add(item)
	}


	move(oldIdx, newIdx)
	{
		let items = Object.assign({}, this.items)
		items[newIdx] = items[oldIdx]
		delete items[oldIdx]
		this.items = items

		// console.log('item moved from ', oldIdx, 'to', newIdx)
		// console.log(this.items)
	}

	/**
	* Iterator implementation.
	* @function
	* @return [{int|string}, item]
	**/
	[Symbol.iterator]()
	{
		return {
			keys 	: this.keys,
			items 	: this.items,
			length 	: this.length,
			counter : 0,
			next()
			{
				if(this.counter>=this.length)
					return {done : true};

				let idx = this.keys[this.counter++];
				return { value : [idx, this.items[idx]], done: false} ;
			}
		}
	}	

	get keys() {return Object.keys(this.items); }
	get values() { return Object.values(this.items); }
	get length() {return this.keys.length; }

	/**
	* Check if a given key exists
	*
	* @param {string} key - lookup key
	* @return {boolean}
	*/
	has(key){ return (key in this.items); }


	/**
	* Retrieve an Item specified by a key
	*
	* @param {string} key - lookup key
	* @return {mixed}
	*/
	get(key){ return this.items[key]; }


	/**
	* Check if Collection is empty
	* @return {boolean}
	*/
	isEmpty(){ return (this.length == 0); }


	clear() {
		this.items = {};
		return this;
	}

	toJSON()
	{
		var arr = [];
		for(let [idx, item] of this)
			arr.push(item);

		return arr;
	}
}

module.exports = Collection;