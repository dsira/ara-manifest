const Property = require('./Property')
const Collection = require('../Collection')
const CustomProperty = require('./CustomProperty')

class Properties extends require('../ManifestObject')
{

	constructor(node="cross")
	{
		super({ node : node });
		this.Property = new Collection({}, 'name');
	}

	addProperty(property)
	{
		if(!property instanceof Property)
			throw new Exception('Property expected');

		this.Property.add(property);

		return this;
	}

	add (property)
	{
		return this.addProperty(property)
	}

	setProperty(name, value)
	{
		if(this.Property.has(name))
			this.Property.get(name).value = value;
		else
			this.Property.add(new Property(name, value));

		return this;
	}

	static fromJSON (json) {
		let instance = new this(json.attributes.node)
		if (json.hasOwnProperty('children')) {
			json.children.forEach(property => {
				let propertyName = property['#name']
				if (propertyName === 'Property') {
					instance.addProperty(Property.fromJSON(property))
				} else {
					instance.addProperty(CustomProperty[propertyName].fromJSON(property))
				}
			})
		}
		return instance
	}
}

module.exports 			= Properties
module.exports.Property = Property
module.exports.CustomProperty = CustomProperty