/**
* @module applicationProperties/Environment
* @require ManifestObject
* @requires Collection
* @requires applicationProperties/Email
* @requires applicationProperties/Properties
* @requires applicationProperties/EmailNotification
*/

const Email = require('./Email'),
	Properties = require('./Properties'),
	Collection = require('../Collection'),
	EmailNotification = require('./EmailNotification');

/**
* Represent Environement 
*
*/
class Environment extends require('../ManifestObject')
{
	/**
	* @constructor
	**/
	constructor(name = "cross")
	{	
		super({ name : name});
		
		this.EmailNotification 	= new EmailNotification;
		this.Properties			= new Collection({}, 'node', true);
		// this.Properties			= new Properties;
	}

	addEmail(destination, name=null)
	{
		this.EmailNotification.addEmail(destination, name);
		return this;
	}

	addProperties(properties)
	{ 

		if(!properties instanceof Properties)
			throw new Exception('Properties expected');

		this.Properties.add(properties); 

		return this;
	}
	
	static fromJSON (json) {
		let instance = new this(json.attributes.name)
		let hasCross = false

		if (json.hasOwnProperty('children')) {
			json.children.forEach(child => {
				let childType = child['#name']

				if (childType === 'EmailNotification') {
					if (child.hasOwnProperty('children')) {
						child.children.forEach(mail => {
							instance.addEmail(mail.attributes.destination, mail.attributes.name)
						})
					} else {
						instance
							.addEmail('TMA', ' ')
							.addEmail('RA', ' ')
							.addEmail('MOA', ' ')
					}
				} else if (childType === 'Properties') {
					instance.addProperties(Properties.fromJSON(child))
					hasCross |= (child.attributes.node === 'cross')
				}
				else
					this.throwMalformedException()
			})
		}

		if (!hasCross) {
			instance.addProperties(new Properties)
		}
		return instance
	}

}


module.exports 				= Environment;
/**@class **/
module.exports.Properties 	= Properties;

