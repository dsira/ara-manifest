/**
* @module applicationProperties
* @require ManifestObject
* @requires Collection
* @requires applicationProperties/Environment
*/

const Environment = require('./Environment'),
	Collection = require('../Collection');

/**
* Represents an applicationProperties manifest Object.
* @extends ManifestObject
* @inheritdoc
*/
class applicationProperties extends require('../ManifestObject') {

	/**
	* @constructor
	*/
	constructor(empty=false)
	{
		super();
		this.Environments = new Collection({}, 'name', true);

		if (!empty) {
			this.add('cross')
		}
	}

	/**
	* Add an Environment
	* @param {Environment} environment - An Environment Object identified by [name] attribute
	*/
	addEnvironment(environment)
	{ 

		if(!(environment instanceof Environment))
			throw new Exception('Environment expected');


		this.Environments.add(environment); 

		return this;
	}

	add(envName) {
		return this.addEnvironment(new Environment(envName))
	}

	static fromJSON (json) {
		let app = json.applicationProperties
		let instance = new this(true)
		let hasCross = false

		try {
			if (app.hasOwnProperty('children')) {
				app.children.forEach(env => {
					if (env['#name'] !== 'Environment') {
						this.throwMalformedException()
					}

					hasCross |= (env.attributes.name === 'cross')
					instance.addEnvironment(Environment.fromJSON(env))
				})
			}
		} catch (e) {
			throw new SyntaxError(e.message ? e.message : 'Malformed XML')
		}

		if (!hasCross) {
			instance.add('cross')
		}

		return instance
	}
	
}



module.exports = applicationProperties;
/** @class **/
module.exports.Environment = Environment;