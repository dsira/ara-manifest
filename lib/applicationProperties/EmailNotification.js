const Collection = require('../Collection'),
	Email = require('./Email');

class EmailNotification extends require('../ManifestObject')
{
	constructor()
	{
		super()
		this.Email = new Collection({}, 'destination');
	}

	addEmail(destination, name=null)
	{ 
		if(destination instanceof Email) 
			var email = destination;
		else if(destination && name)
			var email = new Email(destination, name);

		if(email != void 0)
			this.Email.add(email); 

		return this;
	}
}

module.exports = EmailNotification;