class Instruction extends require('../Property') {
    
    constructor(name, content='')
    {
        super(name)
        this.setData(content)
    }

    static fromJSON (json) {
        return new this(json.attributes.name, json.content)
    }
}

module.exports = Instruction