const Collection = require('../../Collection')


class Question extends require('../../ManifestObject') {
    constructor(text, value)
    { 
        super({text, value})
    }
}


class Jobset extends require('../Property') {
    constructor(name)
    {
        super(name)
        this.Questions = new Collection
    }

    addQuestion(text, value){
        this.Questions.add(new Question(text, value))
        return this
    }

    static fromJSON (json) {
        let attributes = json.attributes
        let instance = new this(attributes.name)
        delete attributes.name

        Object.keys(attributes).forEach(attr => {
            instance.addAttribute(attr, attributes[attr])
        })

        if (json.hasOwnProperty('children')) {
            json.children.forEach(question => {
                instance.Questions.add(Question.fromJSON(question))
            })
        }
        return instance
    }
}

module.exports = Jobset
module.exports.Question = Question