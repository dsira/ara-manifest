module.exports = {
    Script: require('./Script'),
    Instruction: require('./Instruction'),
    Jobset: require('./Jobset')
}
