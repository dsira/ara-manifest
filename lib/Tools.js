/** 
* @module Tools
**/

const fs = require('fs'), Path = require('path'), crypto = require('crypto');


var scanFolder = (folder, relative=true, base=folder) => {
		var 
			files =  fs.readdirSync(folder), 
			res = [];
			
		files.forEach( file => {
			let fullpath = Path.resolve(folder, file),
			stat = fs.statSync(fullpath);

			if(stat && stat.isDirectory())
				res = res.concat(scanFolder(fullpath, relative, base));
			else
				res.push((relative?Path.relative(base, fullpath):fullpath));

		})
		return res;
	}



//crypto.createHash('sha256').update('alice', 'utf8').digest()
function checksum(algo, digest='hex')
{
	return (file) => {

		var fd = fs.readFileSync(Path.resolve(file)),
			hash = crypto.createHash(algo);

		return hash.update(fd).digest(digest);
	}
}








const md5sum = checksum('md5')

function md5sumRecursive(folder)
{
	var checksum = [];

	scanFolder(folder).forEach(file => {
		let sum = md5sum(Path.resolve(folder, file)),
			filename = file.replace(Path.win32.sep, Path.posix.sep);
		checksum.push(`${sum} ${filename}`);
	});

	return checksum;
}


module.exports = {
	scanFolder : scanFolder,
	md5sum : md5sum,
	md5sumRecursive : md5sumRecursive
};