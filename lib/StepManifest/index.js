const Steps = require('./Steps'),
		Environment = require('./Environment');

/**
* Class Representing StepManifest manifest
* @extends ManifestObject
* @module StepManifest
*/
class StepManifest extends require('../ManifestObject')
{
	/**
	 * @param {Boolean} [empty=false] Indicates if instance should be empty
	 */
	constructor(empty=false)
	{
		super();
		this.Steps = new Steps();

		if (!empty) {
			this.Steps.add('cross')
		}
	}

	/**
	 * Add an Environment Object to Steps list
	 * @param {StepManifest/Environment} environment 
	 * @return {StepManifest} this
	 */
	addEnvironment(environment)
	{
		this.Steps.addEnvironment(environment)
		return this
	}

	/**
	 * Add a new Environment specified by name
	 * @param {string} envName Environment Name
	 */
	add(envName)
	{
		this.Steps.add(envName)
		return this
	}

	/**
	 * Instanciate StepManifest from a given object configuration 
	 * @param {object} opts 
	 * @static
	 * @return {StepManifest}
	 */
	static fromJSON(opts)
	{
		let {StepManifest} = opts
		let instance = new this(true)
		let hasCross = false
		try{	
			if (StepManifest.Steps && StepManifest.Steps.length) {
				StepManifest.Steps[0].children.forEach(env => {
					
					if (env['#name'] !== 'Environment')  {
						this.throwMalformedException()
					}

					hasCross |= (env.attributes.name === 'cross')
					instance.addEnvironment(Environment.fromJSON(env, instance.Steps))
				})
			}
		} catch(e) {
			throw new SyntaxError(e.message ? e.message : 'Malformed XML')
		}

		if (!hasCross) {
			instance.add('cross')
		}

		return instance
	}


}

module.exports = StepManifest
module.exports.Step = Steps.Step