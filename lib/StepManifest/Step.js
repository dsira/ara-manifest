const StepDependencies = () => {
	let res = [];
	res.__proto__.toString = function () {
		return this.map(step => step.sequence ).join(',')
	}
	return res;
}

/**
 * Class Representing StepManifest Step
 * @module StepManifest/Step
 */
class Step extends require('../ManifestObject')
{
	constructor(processName, stepName, sequence=0, stage="Deployment", tag="Latest", assign="all", dependency)
	{
		let dependencies = StepDependencies()
		if ( Array.isArray(dependency)) {
			dependency.forEach( item => {
				dependencies.push(item)
			})
		}
		super({
				processName : processName,
				stepName : stepName,
				sequence : sequence,
				stage : stage,
				tag : tag,
				assign : assign,
				dependency : dependencies
		});
	}

	/**
	 * Isntanciate Step Object from a given configuration object
	 * @param {Object} opts  
	 * @static
	 */
	static fromJSON(opts)
	{
		let { processName, stepName, sequence, stage, tag, assign, dependency } = opts.attributes
		return new this(processName, stepName, sequence, stage, tag, assign, dependency !=='' ?dependency.split(','):'')
	}
	
}

module.exports = Step;
module.exports.StepDependencies = StepDependencies;