const Step = require('./Step')
const Collection = require('../Collection')
const Environment = require('./Environment')

/**
 * Steps Collection
 * @extends ManifestObject
 */
class Steps extends require('../ManifestObject')
{
		constructor()
		{
			super();
			this.Environments = new Collection({}, 'name', true)
		}

		/**
		 * 
		 * @param {*} environment 
		 */
		addEnvironment(environment)
		{
			this.Environments.add(environment)
			return this
		}

		add(envName)
		{
			return this.addEnvironment(new Environment(envName,	this))
		}

		get(envName)
		{
			return this.Environments.get(envName)
		}

		getCrossSteps() {
			let crossEnv = this.get('cross')
			if (!crossEnv) {
				return null
			}
			return crossEnv.Steps
		}
}

module.exports = Steps;
module.exports.Step = Step;