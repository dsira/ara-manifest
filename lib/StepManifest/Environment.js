const Step = require('./Step')
const Collection = require('../Collection')
const __globalSteps = Symbol("globalSteps")

//this.__globalSteps = globalSteps


/**
* Represent StepManifest Environment 
* @extends ManifestObject
* @module StepManifest/Environment
*/
class Environment extends require('../ManifestObject')
{
	/**
    * @constructor
    * @param {string} [name=cross] Environement name
	**/
	constructor(name = "cross", globalSteps = null)
	{	
        super({name})
        this[__globalSteps] = globalSteps
		this.Steps	= new Collection()
	}

    get globalSteps () {
        return this[__globalSteps]
    }

    /**
     * Add Step to Steps list
     * @param {Environment/Step} step 
     * @return {Environement} this
     */
    addStep(step)
    {
        this.Steps.add(step)
        return this
    }

    /**
     * Add a new Step defined by given object
     * @param {Object} obj 
     * @return {Environement} this 
     */
    add(obj)
    {
        let {processName, stepName, sequence=0, stage="Deployment", tag="Latest", assign="all", dependency=""} = obj
        return this.addStep(new Step(processName, stepName, sequence, stage, tag, assign, dependency))
    }

	/**
	 * Instanciate Environment from a given object configuration 
	 * @param {object} json 
	 * @static
	 * @return {Environement}
	 */
    static fromJSON(json, globalSteps = null)
    {
        let instance = new this(json.attributes.name, globalSteps)
        
        // let steps = {}
        if (json.children) {
            json.children.forEach(child => {
                if (child['#name'] !== 'Step') {
                    this.throwMalformedException()
                }
                let step = Step.fromJSON(child)
                instance.addStep(step)

                // steps[step.stage] = (steps[step.stage]||[])
                // steps[step.stage].push(step)
            })
        }
        let crossSteps = (instance.name === 'cross') ? instance.Steps.values : [...instance.globalSteps.getCrossSteps().values, ...instance.Steps.values]
        let steps = crossSteps
                    .reduce((res, step) => {
                        let stage = step.stage
                        res[stage] = (res[stage] || [])
                        res[stage].push(step)
                        return res
                    }, {})

        Array.from(instance.Steps).forEach( item => {
            let [,step] = item
            if ( step.dependency.length ) {
                let dependencies = [...step.dependency]
                dependencies.forEach( seq => { 
                    // step.dependency.push(instance.Steps.get(seq-1))
                    let dep = steps[step.stage][seq-1]
                    if (dep) {
                        step.dependency.push(dep)
                    }
                    step.dependency.shift()
                })
            }

        })
    
        return instance
    }
}

module.exports = Environment