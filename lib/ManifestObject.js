const XMLBuilder = require('xmlbuilder')
const Collection = require('./Collection')
const XMLParser = require('xml2js').parseString
// const XMLParser = require('xml-parser')

/**
* Represent a Manifest Object
*/
class ManifestObject {
	
	/**
	* @constructor
	* @param {PlainObject} attrs - Object attributes definition
	*/
	constructor(attrs={})
	{
		this.$ = attrs;

		for(let i in attrs)
		{
			if(attrs.hasOwnProperty(i))
			{
				this.addAttribute(i)
				
			}
		}

		this.$data = null
		this.$comment = null
	}

	addAttribute(attrName, value=undefined)
	{
		if (!this.$.hasOwnProperty(attrName)) {
			this.$[attrName] = value
		} 

		let initialconf = Object.getOwnPropertyDescriptor(this, attrName), conf = {};

		//if no getter already defined
		if(initialconf == undefined || initialconf.get == undefined )
			conf.get = () => { return this.$[attrName]; };

		//if no setter already defined
		if(initialconf == undefined || initialconf.set == undefined )
			conf.set = (v) => { return this.$[attrName]=v; };

		Object.defineProperty(this, attrName, conf);

		return this
	}

	setData(content) {
		this.$data = content
		return this
	}

	getData() {
		return this.$data
	}


	setComment(comment) {
		this.$comment = comment
		return this
	}


	getComment()
	{
		return this.$comment
	}

	/**
	* Generate xml
	* @see {@link https://www.npmjs.com/package/xmlbuilder|xmlbuilder}
	* @return {XMLDoc}
	*/
	toXML(rootNode=null)
	{

		let attrs = Object.assign({}, this.$)
		for(let attr in attrs) {
			if (attrs[attr] === undefined)
				//attrs[attr] = ''
				delete attrs[attr]
		}

		if(rootNode != null)
			var xml = rootNode.ele(this.constructor.name, attrs);
		else
			var xml = XMLBuilder.create(this.constructor.name, attrs);

		Object.keys(this).forEach(objProp => {

			if(objProp=='$' || (!this.hasOwnProperty(objProp)) || objProp.match(/^__/))
				return;

			if(objProp === '$data')
				return this[objProp]?xml.dat(this[objProp]):xml
			
			if(objProp === '$comment')
				// return this[objProp]?xml.commentBefore('COMMENT: ' + this[objProp]).commentAfter('COMMENT END'):xml
				return this[objProp]?xml.commentBefore(this[objProp]):xml

			if(this[objProp] instanceof Collection && !(this[objProp].isEmpty()))
			{
				for(let [idx, item] of this[objProp])
					if('toXML' in item)
						item.toXML(xml);
			}
			else if(this[objProp] !== undefined && 'toXML' in this[objProp])
			{
				this[objProp].toXML(xml);
			}
			else if (this[objProp] === undefined)
			{
				console.warn(`[WARN] ${objProp} is undefined`)
			}

		})

		return xml;
	}

	static fromXML(xmlContent, opt = {})
	{
		let parseOpt = Object.assign({
			attrkey:'attributes',
			explicitChildren: true,
			childkey: 'children',
			preserveChildrenOrder: true,
			explicitArray: true,
			charkey: 'content'
		}, opt)
		return new Promise((resolve, reject) => {
			XMLParser(xmlContent,
			parseOpt, 
			(err, result) => {
				if (err) 
					reject(err)
				else {
					try {
						let instance = this.fromJSON(result)
						resolve(instance)
					} catch (e) {
						reject(e)
					}
				}
			})
			
		})
	}

	/**
	* ToJSON override
	*/
	toJSON()
	{
		if(Object.keys(this.$) == 0)
		{
			let obj = Object.assign({}, this);
			delete obj.$;

			return obj;
		}

		return this;
	}

	static fromJSON(opts)
	{
		console.log(opts)
		// return Function.bind.apply(this, Object.values(opts.attributes))
		let constructor = this.bind(null, ...Object.values(opts.attributes))
		return new constructor
	}

	static throwMalformedException () {
		throw new SyntaxError('Malformed XML')
	}

}


module.exports = ManifestObject;