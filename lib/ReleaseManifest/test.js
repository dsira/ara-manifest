const ReleaseManifest = require('.'),
	Component = require('./Component'),
	Artifact = require('./Artifact'),
	Tools = require('../Tools'),
	path = require('path');




let manifest = new ReleaseManifest(),
	components = {}; // = new Component('ATLAS');





manifest.addApplicationPropertiesFile(new Component.ApplicationPropertiesFile("applicationProperties.xml"));


[
"CDSBB",
"CDSBT",
"CDSJ",
"CDSS",
"CDSU",
"CDSM",
"CDSB",
"CDF"
].forEach( componentName => {
	manifest.addComponent(new Component(componentName));
});




//component.addApplicationPropertiesFile(new Component.ApplicationPropertiesFile("applicationProperties.xml"))


Tools.scanFolder('../../../MA-DAC1301714M/delivery')
		.forEach( file => {
				let info = path.parse(file);
				manifest
					.getComponent(info.dir.toUpperCase())
					.addArtifact(new Artifact({ 
												filename : info.base, 
												//type : info.dir, 
												path : path.posix.join('delivery',info.dir) 
											}));
					//.addArtifact(new Artifact(info.base, info.base, info.dir, path.posix.join('delivery',info.dir)));
			});

//console.log(JSON.stringify(manifest, "", 20))


console.log(manifest.toXML().end({ pretty : true }))




//console.log(Tools.md5sumRecursive('../../../MA-DAC1301714M/delivery'));