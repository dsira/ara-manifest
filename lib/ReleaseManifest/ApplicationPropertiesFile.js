const Collection = require('../Collection');

class ApplicationPropertiesFile extends require('../ManifestObject')
{
	constructor(name, path='.')
	{
		super({ name: name, path : path });
	}

	static fromJSON({ApplicationPropertiesFile}) {
		let {name, path} = ApplicationPropertiesFile.attributes
		return new this(name, path)
	}
}

module.exports = ApplicationPropertiesFile;