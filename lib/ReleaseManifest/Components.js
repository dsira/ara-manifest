const Collection = require('../Collection'),
	Component = require('./Component'),
	ApplicationPropertiesFile = require('./ApplicationPropertiesFile');

class Components extends require('../ManifestObject')
{

	constructor()
	{
		super();
		this.ApplicationPropertiesFile = new Collection({}, 'name');
		this.Component = new Collection({}, 'name');
	}

	addApplicationPropertiesFile(appFile)
	{
		if(!appFile instanceof ApplicationPropertiesFile)
			throw new Exception('ApplicationPropertiesFile expected');

		this.ApplicationPropertiesFile.add(appFile);

		return this;
	}

	addComponent(component)
	{
		if(!component instanceof Component)
			throw new Exception('Component expected');

		this.Component.add(component);

		return this;
	}

}


module.exports = Components;
module.exports.Component = Component;