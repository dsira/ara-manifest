const Path = require('path');

class Artifact extends require('../ManifestObject')
{
	//constructor(filename, name, type, path='.', version="1")

	/**
	* @params {Object} usrOpt - define Artifact properties. At least filename or name must be provided
	* @constructor
	*/
	constructor(usrOpt={})
	{

		if(usrOpt.filename === void 0)
		{
			if(usrOpt.name === void 0)
				throw new Exception('attributes filename or name must be defined');
		}
		else
		{
			let extension = Path.extname(usrOpt.filename);

			if(usrOpt.type === void 0 && extension != '')
			{
				usrOpt.name = Path.basename(usrOpt.filename, extension);
				usrOpt.type = extension.replace('.', '').toLowerCase();
				delete usrOpt.filename;
			} else if (usrOpt.name == void 0) {
				usrOpt.name = usrOpt.filename
			}
		}	


		//merge with default options

		let opt = Object.assign({ path : '.', version: '1', type: ''}, usrOpt);


		//super({filename : filename, name : name, type: type, path: path, version: version});
		super(opt);
	}

	static fromJSON({Artifact}) {
		return new this(Artifact.attributes)
	}
}

module.exports = Artifact;