const Collection = require('../Collection'),
	ApplicationPropertiesFile = require('./ApplicationPropertiesFile'),
	Artifacts = require('./Artifacts');

class Component extends require('../ManifestObject')
{
	constructor(name)
	{
		super({name : name })
		
		this.ApplicationPropertiesFile = new Collection({}, 'name');
		this.Artifacts = new Artifacts;
	}


	addApplicationPropertiesFile(appFile)
	{
		if(!appFile instanceof ApplicationPropertiesFile)
			throw new Exception('ApplicationPropertiesFile expected');

		this.ApplicationPropertiesFile.add(appFile);

		return this;
	}

	addArtifact(artifact)
	{
		this.Artifacts.addArtifact(artifact);

		return this;
	}

	static fromJSON({ Component }) {
		
		let instance = new this(Component.attributes.name)
		if (Component.hasOwnProperty('Artifacts')) {
			let artifacts = Component.Artifacts[0]
			if (artifacts.hasOwnProperty('children') && artifacts.children.length) {
				artifacts.children.forEach(Artifact => {
					let artifact = Artifacts.Artifact.fromJSON({ Artifact })
					if (artifact.type === '') {
					  artifact.type = instance.name.toLowerCase()
					}
					instance.addArtifact(artifact)
				})
			}
		}

		return instance
	}
}

module.exports = Component;
module.exports.ApplicationPropertiesFile = ApplicationPropertiesFile;
module.exports.Artifacts = Artifacts;