const Collection = require('../Collection'),
	Components = require('./Components'),
	ApplicationPropertiesFile = require('./ApplicationPropertiesFile');

class ReleaseManifest extends require('../ManifestObject')
{
	constructor(version="1.0")
	{
		super({ version : version})

		this.Components = new Components;
	}

	/**
	* Add Component 
	*/
	addComponent(component)
	{
		this.Components.Component.add(component);

		return this;
	}

	/**
	* Retrieve given Component
	* @return {Component}
	*/
	getComponent(componentName)
	{
		return this.Components.Component.get(componentName);
	}



	addApplicationPropertiesFile(appFile)
	{
		this.Components.addApplicationPropertiesFile(appFile);

		return this;
	}

	static fromJSON({ReleaseManifest}) {
		let instance = new this()
		let children = ReleaseManifest.Components[0]

		if (children.hasOwnProperty('ApplicationPropertiesFile')) {
			children.ApplicationPropertiesFile.forEach(child => {
				instance.addApplicationPropertiesFile(ApplicationPropertiesFile.fromJSON({ApplicationPropertiesFile: child}))
			})
		}

		if (children.hasOwnProperty('Component')) {
			children.Component.forEach(child => {
				instance.addComponent(Components.Component.fromJSON({Component: child}))
			})
		}
		return instance
	}
}

module.exports = ReleaseManifest;