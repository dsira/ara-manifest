const Collection = require('../Collection'),
	Artifact = require('./Artifact');

class Artifacts extends require('../ManifestObject')
{
	constructor()
	{
		super();
		this.Artifact = new Collection({});
	}


	addArtifact(artifact)
	{
		if(!artifact instanceof Artifact)
			throw new Exception('Artifact expected');

		this.Artifact.add(artifact);

		return this;
	}
}


module.exports = Artifacts;
module.exports.Artifact = Artifact;