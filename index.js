var ARAManifest = module.exports = {
	applicationProperties	: require('./lib/applicationProperties'),
	StepManifest 			: require('./lib/StepManifest'),
	ReleaseManifest 		: require('./lib/ReleaseManifest'),
	version					: require('./package.json').version
};
