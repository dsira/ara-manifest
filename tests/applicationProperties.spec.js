'use strict'

const StepManifest = require('..').StepManifest
const expect = require('chai').expect
const applicationProperties = require('../').applicationProperties
const Property = require('../lib/applicationProperties/Property')
const Script = require('../lib/applicationProperties/CustomProperty/Script')
const Instruction = require('../lib/applicationProperties/CustomProperty/Instruction')
const Jobset = require('../lib/applicationProperties/CustomProperty/Jobset')

describe('ApplicationProperties', () => {
	
	let xmlHeader = `<?xml version="1.0"?>
<applicationProperties>`
	let xmlFooter = `</applicationProperties>`
	let  app = new applicationProperties()
	let envs = {}
  let outputData = {
  envCross: `  <Environment name="cross">
    <EmailNotification>
      <Email destination="TMA" name="addr@bnpparibas.com"/>
      <Email destination="RA" name="addr@bnpparibas.com"/>
      <Email destination="MOA" name="addr@bnpparibas.com"/>
    </EmailNotification>
  </Environment>`
  }

  const testData = () => {
    expect(app.toXML().end({pretty: true})).to.equal(xmlHeader + '\n' + Object.keys(outputData).map(key => outputData[key]).join('\n') + '\n' + xmlFooter)
  }

	describe('Construction', () => {

		it('Should output well formed emailNotification block', () => {
 
			envs.cross = new applicationProperties.Environment()
			envs.cross
				.addEmail("TMA", "addr@bnpparibas.com")
				.addEmail("RA", "addr@bnpparibas.com")
				.addEmail("MOA", "addr@bnpparibas.com")

			app.addEnvironment(envs.cross)
			testData()
		})

		it('Should output well formed new environment with cross node properties', () => {

      outputData.envPROD = `  <Environment name="PRODUCTION">
    <EmailNotification/>
    <Properties node="cross"/>
  </Environment>`

			envs.PRODUCTION = new applicationProperties.Environment('PRODUCTION')
			envs.PRODUCTION.addProperties(new applicationProperties.Environment.Properties())
      app.addEnvironment(envs.PRODUCTION)
      testData()
		})

    it('Should add custom node properties', () => {

       outputData.envPROD = `  <Environment name="PRODUCTION">
    <EmailNotification/>
    <Properties node="cross"/>
    <Properties node="s00v0123456">
      <Property name="Environment Name" value="sa1"/>
      <Property name="EAD Name" value="adoat"/>
      <Instruction name="Amelia - Checkpoint">
        <![CDATA[#Passer les commandes suivantes pour vérification de l'état après installation :
      - dans /src taper la commande zgrep GDI QEP*
      - dans /src taper la commande zgrep GDI QET*
      - dans /jcl taper la commande zgrep GDI jqse*
      - dans /jcl taper la commande zgrep GDI zqse*
      - dans /jcl taper la commande zgrep GDI dqse*
      - dans /jobset taper la commande zgrep GDI jqe*
      - dans /jobset taper la commande zgrep GDI zqe*
      - dans /jobset taper la commande zgrep GDI dqe*
      #Récupérer les résultats et les transmettre par mail au patrimoine.]]>
      </Instruction>
      <Script name="Execution script test">
        <![CDATA[echo "00000000" > $A2_FIC/VMV_PJNUM201]]>
      </Script>
      <Jobset name="mvw1" autoscan="yes">
        <Question text="Entrez le suffixe des jobsets" value="Maro"/>
        <Question text="Nom du user de lancement des jcl" value="atlas"/>
      </Jobset>
    </Properties>
  </Environment>`

      
      let props = new applicationProperties.Environment.Properties('s00v0123456')
      props
						.setProperty("Environment Name", "sa1")
						.addProperty(new applicationProperties.Environment.Properties.Property("EAD Name", "adoat"))
            .add(new Instruction('Amelia - Checkpoint', `#Passer les commandes suivantes pour vérification de l'état après installation :
      - dans /src taper la commande zgrep GDI QEP*
      - dans /src taper la commande zgrep GDI QET*
      - dans /jcl taper la commande zgrep GDI jqse*
      - dans /jcl taper la commande zgrep GDI zqse*
      - dans /jcl taper la commande zgrep GDI dqse*
      - dans /jobset taper la commande zgrep GDI jqe*
      - dans /jobset taper la commande zgrep GDI zqe*
      - dans /jobset taper la commande zgrep GDI dqe*
      #Récupérer les résultats et les transmettre par mail au patrimoine.`))
          .add(new Script('Execution script test', 'echo "00000000" > $A2_FIC/VMV_PJNUM201'))
          .add(new Jobset('mvw1')
                    .addAttribute('autoscan', 'yes')
                    .addQuestion('Entrez le suffixe des jobsets', 'Maro')
                    .addQuestion('Nom du user de lancement des jcl', 'atlas')
                  )
      envs.PRODUCTION.addProperties(props)
      testData()
    })

  })

  describe('XML loading', () => {
    let app2    
    before(() => {
      let appXML = `<?xml version="1.0"?>
      <applicationProperties>
        <Environment name="cross">
          <EmailNotification>
            <Email destination="TMA" name="addr@bnpparibas.com"/>
            <Email destination="RA" name="addr@bnpparibas.com"/>
            <Email destination="MOA" name="addr@bnpparibas.com"/>
          </EmailNotification>
        </Environment>
        <Environment name="PRODUCTION">
          <EmailNotification/>
          <Properties node="cross"/>
          <Properties node="s00v0123456">
            <Property name="Environment Name" value="sa1"/>
            <Property name="EAD Name" value="adoat"/>
            <Instruction name="Amelia - Checkpoint">
              #Passer les commandes suivantes pour vérification de l'état après installation :
            - dans /src taper la commande zgrep GDI QEP*
            - dans /src taper la commande zgrep GDI QET*
            - dans /jcl taper la commande zgrep GDI jqse*
            - dans /jcl taper la commande zgrep GDI zqse*
            - dans /jcl taper la commande zgrep GDI dqse*
            - dans /jobset taper la commande zgrep GDI jqe*
            - dans /jobset taper la commande zgrep GDI zqe*
            - dans /jobset taper la commande zgrep GDI dqe*
            #Récupérer les résultats et les transmettre par mail au patrimoine.
            </Instruction>
            <Script name="Execution script test">
              echo "00000000" > $A2_FIC/VMV_PJNUM201
            </Script>
            <Jobset name="mvw1" autoscan="yes">
              <Question text="Entrez le suffixe des jobsets" value="Maro"/>
              <Question text="Nom du user de lancement des jcl" value="atlas"/>
            </Jobset>
          </Properties>
        </Environment>
      </applicationProperties>`

      return applicationProperties
        .fromXML(appXML)
        .then(manifest => {
          app2 = manifest
          return Promise.resolve(app2)
        })
    })

    it('Should instanciate 2 Environements', () => {
      expect(app2.Environments.length).equal(2)
      expect(app2.Environments.get('cross')).to.be.an.instanceOf(applicationProperties.Environment)
      expect(app2.Environments.get('PRODUCTION')).to.be.an.instanceOf(applicationProperties.Environment)
    })

    it('Should instanciate 3 emails', () => {
      let email = app2.Environments.get('cross').EmailNotification.Email
      expect(email.length).equal(3)
      expect(email.get('TMA').name).to.be.equal('addr@bnpparibas.com')
      expect(email.get('RA').name).to.be.equal('addr@bnpparibas.com')
      expect(email.get('MOA').name).to.be.equal('addr@bnpparibas.com')      
    })

   describe('Production env', () => {
      let envPROD, properties 
      it('Should contains 2 nodes', () => {
        envPROD = app2.Environments.get('PRODUCTION')
        expect(envPROD.Properties.length).to.be.equal(2)
        properties = envPROD.Properties.get('s00v0123456').Property
      })

      describe('Node s00v0123456 ', () => {

        it('should contains 5 Properties', () => {
          expect(properties.length).to.be.equal(5)
        })

        it('2 standards properties', () => {
          expect(properties.get('Environment Name')).to.be.an.instanceOf(Property)
          expect(properties.get('EAD Name')).to.be.an.instanceOf(Property)
        })

        it('Instruction', () => {
          let instruction = properties.get('Amelia - Checkpoint')
          expect(instruction).to.be.an.instanceOf(Instruction)
          expect(instruction.getData()).to.have.string('zgrep GDI jqe*')
        })

        it('Script', () => {
          let script = properties.get('Execution script test')
          expect(script).to.be.an.instanceOf(Script)
          expect(script.getData()).to.have.string('$A2_FIC/VMV_PJNUM201')
        })
        
        it('Jobset', () => {
          let jobset = properties.get('mvw1')
          expect(jobset).to.be.an.instanceOf(Jobset)
          expect(jobset.Questions.length).to.be.equal(2)

          let question = jobset.Questions.get(0)
          expect(question).to.be.an.instanceOf(jobset.constructor.Question)
          expect(question.text).to.be.equal('Entrez le suffixe des jobsets')
          expect(question.value).to.be.equal('Maro')

        })

      })

    })
  })
})