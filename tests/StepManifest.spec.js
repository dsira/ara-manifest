'use strict'

// import { StepManifest } from '..'
const StepManifest = require('..').StepManifest
const expect = require('chai').expect

const isError = e => (typeof e === 'string') ? Promise.reject(new Error(e)) : Promise.resolve(e)

describe('StepManifest', () => {
  describe('XML loading', () => {

    it('should validate XML', () => {
      let xml = `<?xml version="1.0"?>
<StepManifest>
  <Steps>
    <Environment name="cross">
      <Foo bar="foo"/>
    </Environment>
  </Steps>
</StepManifest>`

      return StepManifest
        .fromXML(xml)
        .then(() => {
          return Promise.reject('Expected method to reject')
        })
        .catch(isError)
        .then(e => {          
          expect(e.message).to.equal('Malformed XML')
          return Promise.resolve(e)
        })
    })

    it('should load StepManifest properly', () => {
      let xml =`<?xml version="1.0"?>
<StepManifest>
  <Steps>
    <Environment name="cross">
      <Step processName="Execution Scripts Pre-deploiement" stepName="Execution Scripts Pre-deploiement" sequence="1" stage="Deployment" tag="Latest" assign="all" dependency=""/>
      <Step processName="Livraison Config TP" stepName="Livraison Config TP" sequence="2" stage="Deployment" tag="Latest" assign="all" dependency=""/>
      <Step processName="Livraison Composants" stepName="Livraison Composants" sequence="3" stage="Deployment" tag="Latest" assign="all" dependency=""/>
      <Step processName="Action manuelle" stepName="Installation GDI 555545" sequence="4" stage="Deployment" tag="Latest" assign="all" dependency="3"/>
      <Step processName="Action manuelle" stepName="Installation GDI 424572" sequence="5" stage="Deployment" tag="Latest" assign="all" dependency="4"/>
      <Step processName="Chargement Jobsets" stepName="Chargement Jobsets" sequence="6" stage="Deployment" tag="Latest" assign="all" dependency="3,5"/>
      <Step processName="Execution Scripts Post-deploiement" stepName="Execution Scripts Post-deploiement" sequence="7" stage="Deployment" tag="Latest" assign="all" dependency=""/>
    </Environment>
    <Environment name="PRODUCTION">
      <Step processName="Action manuelle" stepName="Arrêt de la supervision" sequence="1" stage="Deployment" tag="Latest" assign="all" dependency=""/>
    </Environment>
  </Steps>
</StepManifest>`

      StepManifest.fromXML(xml)
        .then(manifest => {
          expect(manifest).to.be.instanceOf(StepManifest)
          expect(manifest.toXML().end({pretty: true})).to.be.equal(xml)
          return Promise.resolve(manifest)
        })
        .catch(e => {
          return Promise.reject(e)
        })
      })
  })
})
