'use strict'

const expect = require('chai').expect

const ReleaseManifest = require('..').ReleaseManifest
const Component = require('../lib/ReleaseManifest/Component')
const Artifact  = require('../lib/ReleaseManifest/Artifact')
const ApplicationPropertiesFile  = require('../lib/ReleaseManifest/ApplicationPropertiesFile')

describe('Manifest', () => {
    describe('XML Loading', () => {

    it('Should properly load Artifact from xml fragment', () => {
      return Promise.all([
          Artifact.fromXML(`<Artifact path="delivery/cdf" version="CRQ000000038607" filename="VME_PZERR151_CRQ000000038607" name="VME_PZERR151" type="VME_PZERR151"/>`)
          .then(artifact => {
            expect(artifact).to.be.an.instanceof(Artifact)
            expect(artifact.path).to.equal('delivery/cdf')
            expect(artifact.version).to.equal('CRQ000000038607')
            expect(artifact.filename).to.equal('VME_PZERR151_CRQ000000038607')
            expect(artifact.name).to.equal('VME_PZERR151')
            expect(artifact.type).to.equal('VME_PZERR151')
          }),
          Artifact.fromXML(`<Artifact path="delivery/cdsbb" version="1" name="VM0C2B" type="gnt"/>`)
          .then(artifact => {
            expect(artifact).to.be.an.instanceof(Artifact)
            expect(artifact.path).to.equal('delivery/cdsbb')
            expect(artifact.version).to.equal('1')
            expect(artifact.filename).to.equal(undefined)
            expect(artifact.name).to.equal('VM0C2B')
            expect(artifact.type).to.equal('gnt')
          })
      ])
    })

    it('Should properly load component from xml fragment', () => {
      return Component.fromXML(`<Component name="CDF">
      <Artifacts>
        <Artifact path="delivery/cdf" version="CRQ000000038607" filename="VME_PZERR151_CRQ000000038607" name="VME_PZERR151" type="VME_PZERR151"/>
        <Artifact path="delivery/cdf" version="CRQ000000039225" filename="VME_PZERR151_CRQ000000039225" name="VME_PZERR151" type="VME_PZERR151"/>
      </Artifacts>
    </Component>`)
        .then(component => {
            expect(component).to.be.an.instanceof(Component)
            expect(component.Artifacts.Artifact.length).to.equal(2)
        })
    })

    it('Should properly load applicationPropertiesFile from xml', () => {
      return ApplicationPropertiesFile
        .fromXML(`<ApplicationPropertiesFile name="applicationProperties.xml" path="." />`)
        .then(apf => {
          expect(apf).to.be.an.instanceof(ApplicationPropertiesFile)
          expect(apf.name).to.equal("applicationProperties.xml")
          expect(apf.path).to.equal(".")
        })
    });

    it('Should properly load ReleaseManifest from XMl', () => {
      return ReleaseManifest.fromXML(`
      <ReleaseManifest version="1.0">
          <Components>
            <ApplicationPropertiesFile name="applicationProperties.xml" path="." />
            <Component name="CDSBB">
              <Artifacts>
                <Artifact path="delivery/cdsbb" version="1" name="VM0C2B" type="gnt"/>
                <Artifact path="delivery/cdsbb" version="1" name="VM0C2C" type="gnt"/>
                <Artifact path="delivery/cdsbb" version="1" name="VMP315" type="gnt"/>
                <Artifact path="delivery/cdsbb" version="1" name="VMPEX0" type="gnt"/>
                <Artifact path="delivery/cdsbb" version="1" name="VMPEXR" type="gnt"/>
                <Artifact path="delivery/cdsbb" version="1" name="VMPIM2" type="gnt"/>
                <Artifact path="delivery/cdsbb" version="1" name="VMPIM4" type="gnt"/>
                <Artifact path="delivery/cdsbb" version="1" name="VMPNU2" type="gnt"/>
                <Artifact path="delivery/cdsbb" version="1" name="VMPV20" type="gnt"/>
                <Artifact path="delivery/cdsbb" version="1" name="VMPV5A" type="gnt"/>
                <Artifact path="delivery/cdsbb" version="1" name="VMPV74" type="gnt"/>
                <Artifact path="delivery/cdsbb" version="1" name="VMPW33" type="gnt"/>
                <Artifact path="delivery/cdsbb" version="1" name="VMPW88" type="gnt"/>
                <Artifact path="delivery/cdsbb" version="1" name="VMPWO6" type="gnt"/>
                <Artifact path="delivery/cdsbb" version="1" name="VMPWO7" type="gnt"/>
              </Artifacts>
            </Component>
          </Components>
      </ReleaseManifest>`)
      .then(manifest => {
        expect(manifest).to.be.an.instanceof(ReleaseManifest)
        expect(manifest.Components.ApplicationPropertiesFile.length).to.equal(1)
        expect(manifest.Components.Component.length).to.equal(1)

        //console.log( manifest.toXML().end({pretty:true}))
        let component = manifest.getComponent('CDSBB')
        expect(component).to.be.instanceof(Component)
        expect(component.Artifacts.Artifact.length).to.equal(15)
      })
    });
    
    
    })
})